Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: courier
Upstream-Contact: Sam Varshavchik <mrsam@courier-mta.com>
Source: https://www.courier-mta.org/

Files: *
Copyright: 1992-2019 Double Precision, Inc.
License: GPL-3+

Files: debian/*
Copyright: 2000 Dennis Schoen <dennis@debian.org>
	   2000-2015 Stefan Hornburg <racke@linuxia.de>
	   2015 Ondřej Surý <ondrej@debian.org>
	   2017-2020 Markus Wanner <markus@bluegap.ch>
License: GPL-3+

Files: debian/po/cs.po
Copyright: 2009,2019 Martin Sin <martin.sin@zshk.cz>
License: GPL-3+

Files: debian/po/da.po
Copyright: 2010,2019 Joe Hansen <joedalton2@yahoo.dk>
License: GPL-3+

Files: debian/po/de.po
Copyright: 2004 Stefan Hornburg (Racke) <racke@linuxia.de>
           2007, 2008, 2017 Holger Wansing <linux@wansing-online.de>
	   2019 Holger Wansing <hwansing@mailbox.org>
License: GPL-3+

Files: debian/po/es.po
Copyright: 2001, 2003 Carlos Valdivia Yagüe <valyag@dat.etsit.upm.es>
           2008 Francisco Javier Cuadrado <fcocuadrado@gmail.com>
	   2019 Jonatan Porras <jonatanpc8@gmail.com>
License: GPL-3+

Files: debian/po/eu.po
Copyright: 2008 Piarres Beobide <pi@beobide.net>
           2019 Iñaki Larrañaga Murgoitio <dooteo@zundan.com>
License: GPL-3+

Files: debian/po/fi.po
Copyright: 2008 Esko Arajärvi <edu@iki.fi>
License: GPL-3+

Files: debian/po/fr.po
Copyright: 2006, 2007, 2008 Christian Perrier <bubulle@debian.org>
           2017 Alban Vidal <alban.vidal@zordhak.fr>
License: GPL-3+

Files: debian/po/gl.po
Copyright: 2007, 2008 Jacobo Tarrio <jtarrio@debian.org>
License: GPL-3+

Files: debian/po/it.po
Copyright: 2008,2019 Luca Monducci <luca.mo@tiscali.it>
License: GPL-3+

Files: debian/po/ja.po
Copyright: 2008,2018 Hideki Yamane (Debian-JP) <henrich@debian.or.jp>
License: GPL-3+

Files: debian/po/nl.po
Copyright: 2008 Bart Cornelis <cobaco@skolelinux.no>
           2016,2019 Frans Spiesschaert <Frans.Spiesschaert@yucom.be>
License: GPL-3+

Files: debian/po/pt_BR.po
Copyright: 2007 André Luís Lopes <andrelop@debian.org>
License: GPL-3+

Files: debian/po/pt.po
Copyright: 2005-2008 Miguel Figueiredo <elmig@debianpt.org>
           2017,2019 Rui Branco <ruipb@debianpt.org>
License: GPL-3+

Files: debian/po/ru.po
Copyright: 2005 Yuriy Talakan' <yt@amur.elektra.ru>
           2007 <yt@drsk.ru>
	   2008 Yuri Kozlov <kozlov.y@gmail.com>
	   2007, 2017 Sergey Alyoshin <alyoshin.s@gmail.com>
License: GPL-3+

Files: debian/po/sv.po
Copyright: 2008 Martin Ågren <martin.agren@gmail.com>
           2019 Martin Bagge <brother@bsnet.se>
License: GPL-3+

Files: debian/po/tr.po
Copyright: 2004 Mehmet Türker <mturker@innova.com.tr>
License: GPL-3+

Files: debian/po/vi.po
Copyright: 2005-2008 Clytie Siddall <clytie@riverland.net.au>
License: GPL-3+

Files: debian/po/zh_CN.po
Copyright: 2019 Yangfl <mmyangfl@gmail.com>
License: GPL-3+

Files: debian/po/zh_TW.po
Copyright: 2019 Yangfl <mmyangfl@gmail.com>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
